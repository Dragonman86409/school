package com.assignment.Servlet;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class Server {

    // Used example code to get basically working

    public static String getContent(String string) {

        String info = "";

        try {
            URL urlString = new URL(string);
            HttpURLConnection http = (HttpURLConnection) urlString.openConnection();

            BufferedReader urlReader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = urlReader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            info = stringBuilder.toString();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return info;
    }

    public static Map getHeader(String string) {
        Map mapping = null;

        try {
            URL urlMap = new URL(string);
            HttpURLConnection http = (HttpURLConnection) urlMap.openConnection();

            mapping = http.getHeaderFields();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return mapping;

    }


}
