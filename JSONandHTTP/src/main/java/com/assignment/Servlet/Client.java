package com.assignment.Servlet;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Client {

    public static String HTTPtoJSON(Server server) {

        ObjectMapper mapper = new ObjectMapper();
        String http = "";

        try {
            http = mapper.writeValueAsString(server);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return http;
    }

    public static void main(String[] args) {
        System.out.println("Enter a website address. (Do not enter www in the address):");
        Scanner in = new Scanner(System.in);
        var urlSite = "";
        urlSite = in.nextLine();

        Map<Integer, List<String>> m = Server.getHeader("http://www." + urlSite);


        for (Map.Entry<Integer, List<String>> website : m.entrySet()) {
            try {
                System.out.println("Key= " + website.getKey() + website.getValue());
            } catch (Exception e) {
                System.err.println(e.toString());
            }
        }
    }

}
