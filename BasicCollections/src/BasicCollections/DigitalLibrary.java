package BasicCollections;

public class DigitalLibrary {

    private String title;
    private String release;

    public DigitalLibrary(String title, String release) {
        this.title = title;
        this.release = release;
    }

    public String toString() {
        return title + " was released in the year " + release + ".";
    }

}
