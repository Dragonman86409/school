package BasicCollections;

import java.util.*;

public class ColletionTypes {

    public static void main(String[] args) {

        System.out.println("-- List --");
        List list = new ArrayList();
        list.add("Tom");
        list.add("Dick");
        list.add("Harry");
        list.add("Bob");
        list.add("Joel");
        list.add("Tommy");

        for (Object str : list) {
            System.out.println((String) str);
        }

        System.out.println("-- Set --");
        Set set = new TreeSet();
        set.add("Tom");
        set.add("Dick");
        set.add("Harry");
        set.add("Bob");
        set.add("Joel");
        set.add("Tommy");

        for (Object str : set) {
            System.out.println((String) str);
        }

        System.out.println("-- Queue --");
        Queue queue = new PriorityQueue();
        queue.add("Tom");
        queue.add("Dick");
        queue.add("Harry");
        queue.add("Bob");
        queue.add("Joel");
        queue.add("Tommy");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        System.out.println("-- Map --");
        Map map = new HashMap();
        map.put(1, "Tom");
        map.put(2, "Dick");
        map.put(3, "Harry");
        map.put(4, "Bob");
        map.put(5, "Joel");
        map.put(1, "Tommy");

        for (int i = 1; i < 6; i++) {
            String result = (String) map.get(i);
            System.out.println(result);
        }

        System.out.println("-- Sample List of Digital Library --");
        List<DigitalLibrary> myList = new LinkedList<DigitalLibrary>();
        myList.add(new DigitalLibrary("10,000 BC", "2008"));
        myList.add(new DigitalLibrary("Dr. Seuss on the Loose", "1973"));
        myList.add(new DigitalLibrary("How to Train Your Dragon: The Hidden World", "2019"));
        myList.add(new DigitalLibrary("Supergirl", "1984"));
        myList.add(new DigitalLibrary("Seven Brides for Seven Brothers", "1954"));
        myList.add(new DigitalLibrary("Teenage Mutant Ninja Turtles", "1990"));
        myList.add(new DigitalLibrary("Sonic the Hedgehog", "2020"));

        for (DigitalLibrary dl : myList) {
            System.out.println(dl);
        }

    }

}
