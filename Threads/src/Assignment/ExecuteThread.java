package Assignment;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteThread {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(3);

        //Using Final Fantasy 7 characters for the assignment
        Thread ds1 = new Thread("Cloud", 100, 1000);
        Thread ds2 = new Thread("Barret", 90, 950);
        Thread ds3 = new Thread("Tifa", 80, 900);
        Thread ds4 = new Thread("Aerith", 60, 500);
        Thread ds5 = new Thread("Vincent", 50, 750);
        Thread ds6 = new Thread("Red XIII", 70, 800);
        Thread ds7 = new Thread("Cait Sith", 40, 400);
        Thread ds8 = new Thread("Cid", 30, 700);
        Thread ds9 = new Thread("Yuffie", 20, 650);
        Thread ds10 = new Thread("Reno", 1, 50); //NPC (usually the enemy). Weak character (according to me)
        Thread ds11 = new Thread("Sephiroth", 150, 1000);

        myService.execute(ds1);
        myService.execute(ds2);
        myService.execute(ds3);
        myService.execute(ds4);
        myService.execute(ds5);
        myService.execute(ds6);
        myService.execute(ds7);
        myService.execute(ds8);
        myService.execute(ds9);
        myService.execute(ds10);
        myService.execute(ds11);

        myService.shutdown();
    }
}