package Assignment;

import java.util.Random;

public class Thread implements Runnable{

    private String name;
    private int number;
    private int sleep;
    private int rand;

    public Thread(String name, int number, int sleep){

        this.name = name;
        this.number = number;
        this.sleep = sleep;

        Random random = new Random();
        this.rand = random.nextInt(1000);

    }



    public void run() {
        System.out.println("\n" + name + " is fighting a random monster. Attack: " + number + " Special: " + rand + "\n");
        for (int count = 1; count < rand; count++) {
            if (name == "Reno"){ //If this name is chosen, this part of the script is activated.
                if (count % number == 0) {
                    System.out.print(name + " attacked the monster. ");
                    System.out.print("Monster is laughing at " + name + " because he's too weak. ");
                    try {
                        java.lang.Thread.sleep(sleep);
                    } catch (InterruptedException e) {
                        System.err.println(e.toString());
                    }
                }
            }

            else if(name == "Sephiroth"){//Main enemy of the game.
                if (count % number == 0) {
                    System.out.print("Planet says " + name + " is a mama's boy. ");
                    System.out.print(name + " get's mad and wants to destroy the planet. ");
                    try {
                        java.lang.Thread.sleep(sleep);
                    } catch (InterruptedException e) {
                        System.err.println(e.toString());
                    }
                }

            } else if (name == "Yuffie") {
                if (count % number == 0) {
                    System.out.print(name + " steals Matria from monster. ");
                    System.out.print(name + " notices that it wasn't Matria. ");
                    try {
                        java.lang.Thread.sleep(sleep);
                    } catch (InterruptedException e) {
                        System.err.println(e.toString());
                    }
                }

            } else if (name == "Tifa") {
                if (count % number == 0) {
                    System.out.print(name + " punches monster. ");
                    System.out.print("Monster is running away because " + name + " punches are too strong. ");
                    try {
                        java.lang.Thread.sleep(sleep);
                    } catch (InterruptedException e) {
                        System.err.println(e.toString());
                    }
                }

            } else if (name == "Cloud") {
                if (count % number == 0) {
                    System.out.print(name + " attacks monster. ");
                    System.out.print("Monster is running away because " + name + " has a big sword. ");
                    try {
                        java.lang.Thread.sleep(sleep);
                    } catch (InterruptedException e) {
                        System.err.println(e.toString());
                    }
                }

            } else {
                if (count % number == 0) {//Other names will go through this part of the script,
                    System.out.print(name + " attacked the monster. ");
                    System.out.print("Monster is running away because " + name + " is too strong. ");
                    try {
                        java.lang.Thread.sleep(sleep);
                    } catch (InterruptedException e) {
                        System.err.println(e.toString());
                    }
                }
            }
        }
        if (name == "Reno") { //If this name is chosen, this part of the script is activated.
            if (rand <= 200){
                System.out.print("\n" + name + " defeated the monster.\n\n");
            }
            else {
                System.out.print("\n" + name + " defeated the monster... finally.\n\n");
            }
        }
        
        else if (name == "Sephiroth") {//Main enemy of the game.
            System.out.print("\n" + name + " is defeated because he is a mama's boy.\n\n");
        }

        else if (name == "Yuffie") {
            System.out.print("\n" + name + " stole all the Matria from monster. Then, she ran away.\n\n");
        }

        else if (name == "Tifa") {
            System.out.print("\n" + name + " punches the monster to death.\n\n");
        }

        else if (name == "Cloud") {
            System.out.print("\n" + name + " defeats monster because of his big sword.\n\n");
        }

        else {
            System.out.println("\n" + name + " has defeated the monster.\n\n");

        }

    }
}
