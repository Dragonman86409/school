package Exception;

import lombok.Value;

import javax.validation.constraints.Min;
import javax.validation.constraints.NegativeOrZero;
import javax.validation.constraints.NotNull;

@Value

public class validate {

    @Min(1)
    @NotNull(message = "Enter a number.")
    double num1;

    @Min(1)
    @NegativeOrZero
    @NotNull(message = "Enter a number greater than 0.")
    double num2;

}
