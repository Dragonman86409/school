package Division;

import java.util.Scanner;

public class division {

    public static void main(String[] args) {

        Scanner num = new Scanner((System.in));
        float num1;
        float num2;
        float result;

        do {
            //Get's numerator for numbers greater or equal to 0
            System.out.println("Enter a number for numerator:");
            num1 = num.nextFloat();
        } while (!(num1 >= 0));
        System.out.println("You picked: " + num1);

        /* can't get this to work to prevent letter from being used
        try {
            Integer.parseInt("0");
        } catch (InputMismatchException e) {
            System.out.println("Please enter a number");
        }*/

        do {
            //Get's denominator for numbers greater than 0
            System.out.println("Enter a number for denominator:");
            num2 = num.nextFloat();
        } while (!(num2 >= 0));

        /* can't get this to work to prevent letter from being used
        try {
            Integer.parseInt("0");
        } catch (InputMismatchException e) {
            System.out.println("Please enter a number");
        }*/

        System.out.println("Your denominator is: " + num2);

        float no1 = num1;
        float no2 = num2;

        result = doDivision(num1, num2);

        System.out.println("Division of " + num1 + " and " + num2 + " is: " + result);
    }

    private static float doDivision(float num1, float num2) throws ArithmeticException {
        return num1 / num2;
    }
}
