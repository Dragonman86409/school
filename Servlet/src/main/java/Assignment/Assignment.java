package Assignment;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/** This is a simple servlet that implements doPost and doGet.  The doPost takes
 *  two parameters (login and password) and displays them.
 */
@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Assignment extends HttpServlet {

    /** this is the main method that uses the two parameters and displays them. */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String login2 = request.getParameter("login2");
        String password2 = request.getParameter("password2");
        out.println("<h1>Welcome to your login page of said application </h1>");
        out.println("<p>Hello " + login2 + ",</p>");
        out.println("<p>You have access to applications on this site.</p>");
        out.println("<p>If you are reading this, you are awesome!</p>");
        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");
        out.println("Try logging in using different credentials.");
    }

}
