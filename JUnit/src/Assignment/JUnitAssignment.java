package Assignment;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

public class JUnitAssignment {

    // Used code from another to set foundation of this assignment
    public static void main(String[] args) {
        System.out.println("Enter first and second number:");
        Scanner inp = new Scanner(System.in);
        int num1, num2;
        num1 = inp.nextInt();
        num2 = inp.nextInt();
        int ans;
        System.out.println("Enter your selection: 1 for Addition, 2 for subtraction 3 for Multiplication and 4 for division:");
        int choose;
        choose = inp.nextInt();
        switch (choose) {
            case 1:
                System.out.println(add(num1, num2));
                break;
            case 2:
                System.out.println(sub(num1, num2));
                break;
            case 3:
                System.out.println(mult(num1, num2));
                break;
            case 4:
                System.out.println(div(num1, num2));
                break;
            default:
                System.out.println("Illegal Operation");


        }


    }

    public static int add(int x, int y) {
        int result = x + y;
        return result;
    }

    public static int sub(int x, int y) {
        int result = x - y;
        return result;
    }

    public static int mult(int x, int y) {
        int result = x * y;
        return result;
    }

    public static int div(int x, int y) {
        int result = x / y;
        return result;
    }

    // My code
    @Nested
    class scanner {
        @Test
        void scannerNull() {
            assertNull("Enter a number");
        }

        @Test
        void scannerNotNull() {
            assertNotNull("Number has been entered");
        }
    }
    @Test
    void mult() {
        assertAll(
                () -> assertEquals(0, mult(1, 0)),
                () -> assertEquals(1, mult(1, 1)),
                () -> assertEquals(6, mult(2, 3))
        );
    }

    @Test
    void div() {
        assertThrows(ArithmeticException.class, () -> div(1, 0),
                "Divide should throw ArithmeticException when denominator is zero");
    }
}
